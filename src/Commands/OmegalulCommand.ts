import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Command, ICommandOption } from "./Command";

export class OmegalulCommand extends Command {
  public commands: Map<string, ICommandOption> = new Map([
    ["omegalul", {
      channelTypes: [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM],
      help: "OMEGALULise le message passé en paramètre.",
    }],
  ]);
  public deleteOriginalMessage: boolean = true;

  protected async process(command: string, args: string[], message: Discord.Message): Promise<boolean> {
    if (args.length < 1) {
      await this.temporaryReplyToMessage(message, "Essaye plutôt sous la forme `/omegalul <message>`.");
      return true;
    }
    const originalMessage = args.join(" ").toLowerCase();
    if (!originalMessage.includes("o")) {
      await this.temporaryReplyToMessage(message,
        "Comment veux-tu OMEGALULiser un message qui ne contient pas d'O ? c'est useless. :sob:");
      return true;
    }
    const emoji = this.bot.client.emojis.find((e) => e.name === "OMEGALUL");
    if (!emoji) {
      await this.temporaryReplyToMessage(message, "Je n'ai pas trouvé d'emoji OMEGALUL sur ce serveur. :sob:");
      return true;
    }
    const omegalulizedMessage = originalMessage.split("").map((l) => l === "o" ? `${emoji}` : l)
                                               .join(" ").toUpperCase();
    await this.replyToMessage(message, omegalulizedMessage);
    return true;
  }
}
