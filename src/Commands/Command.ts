import Discord from "discord.js";
import { Bot } from "../Bot";
import { ChannelType } from "../Discord/ChannelType";
import { Logger } from "../Utils/Logger";

const logger = Logger.getLogger("Command");

export interface ICommandOption {
  roles?: string[];
  channels?: string[];
  channelTypes: ChannelType[];
  help?: string;
}

export abstract class Command {
  public abstract commands: Map<string, ICommandOption> = new Map();
  public deleteOriginalMessage: boolean = false;
  public deleteOriginalMessageAfter: number = 0;
  public temporaryReplyDeleteTimeout: number = 30000;
  protected bot: Bot;

  constructor(bot: Bot) {
    this.bot = bot;
  }

  public isMatched(command: string): boolean {
    return this.commands.has(command);
  }

  public async tryProcess(command: string, args: string[], message: Discord.Message, oldMessage?: Discord.Message)
                          : Promise<boolean> {
    const options = this.commands.get(command);
    if (!options) {
      throw new Error(`Unknown command "${command}".`);
    }
    if (message.channel.type === ChannelType.TEXT && this.deleteOriginalMessage) {
      await message.delete(this.deleteOriginalMessageAfter);
    }
    // Check for channel type condition.
    const channelTypes = options.channelTypes;
    if (channelTypes && channelTypes.length > 0 && !channelTypes.includes(message.channel.type as ChannelType)) {
      await this.temporaryReplyToMessage(message,
        "Tu n'es pas dans le bon type de channel pour exécuter cette commande.");
      return false;
    }
    // Check for roles condition.
    const roles = options.roles;
    if (roles && roles.length > 0 && !message.member.roles.some((r) => roles.includes(r.name))) {
      await this.temporaryReplyToMessage(message, "Tu n'as pas les droits nécessaires pour exécuter cette commande.");
      return false;
    }
    // Check for channel condition.
    const channels = options.channels;
    const messageChannelName = (message.channel as Discord.TextChannel).name;
    if (channels && channels.length > 0 && !channels.includes(messageChannelName)) {
      await this.temporaryReplyToMessage(message, "Tu n'es pas dans le bon channel pour exécuter cette commande.");
      return false;
    }

    return this.process(command, args, message, oldMessage);
  }

  protected abstract async process(command: string, args: string[], message: Discord.Message,
                                   oldMessage?: Discord.Message): Promise<boolean>;

  protected async replyToMessage(originalMessage: Discord.Message, content?: Discord.StringResolvable,
                                 options?: Discord.MessageOptions): Promise<Discord.Message | Discord.Message[]> {
    return originalMessage.reply(content, options);
  }

  protected async replyToChannel(originalMessage: Discord.Message, content?: Discord.StringResolvable,
                                 options?: Discord.MessageOptions): Promise<Discord.Message | Discord.Message[]> {
    return originalMessage.channel.send(content, options);
  }

  protected async temporaryReplyToMessage(originalMessage: Discord.Message, content?: Discord.StringResolvable,
                                          options?: Discord.MessageOptions)
                                          : Promise<Discord.Message | Discord.Message[]> {
    // If we're not in a text channel, don't post temporary messages since it's useless.
    if (originalMessage.channel.type !== ChannelType.TEXT) {
      return this.replyToMessage(originalMessage, content, options);
    }
    const result = await originalMessage.reply(content, options);
    this.registerTemporaryMessages(result);
    return result;
  }

  protected async temporaryReplyToChannel(originalMessage: Discord.Message, content?: Discord.StringResolvable,
                                          options?: Discord.MessageOptions)
                                          : Promise<Discord.Message | Discord.Message[]> {
    // If we're not in a text channel, don't post temporary messages since it's useless.
    if (originalMessage.channel.type !== ChannelType.TEXT) {
      return this.replyToChannel(originalMessage, content, options);
    }
    const result = await originalMessage.channel.send(content, options);
    this.registerTemporaryMessages(result);
    return result;
  }

  protected registerTemporaryMessages(messages: Discord.Message | Discord.Message[]) {
    if (typeof this.temporaryReplyDeleteTimeout === "undefined") {
      return;
    }
    if (messages instanceof Discord.Message) {
      messages = [messages];
    }
    messages.forEach((message) => {
      message.delete(this.temporaryReplyDeleteTimeout).catch((error) => {
        logger.error("Error while deleting posted message.", error);
      });
    });
  }
}
