import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Command, ICommandOption } from "./Command";

export class PingCommand extends Command {
  public commands: Map<string, ICommandOption> = new Map([
    ["ping", {
      channelTypes: [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM],
      help: "Retourne la latence du bot avec le serveur.",
    }],
  ]);
  public deleteOriginalMessage: boolean = true;

  protected async process(command: string, args: string[], message: Discord.Message): Promise<boolean> {
    let botMessage = await this.temporaryReplyToChannel(message, "Pong !");
    if (botMessage instanceof Array) {
      botMessage = botMessage[0];
    }
    const serverLatency = botMessage.createdTimestamp - message.createdTimestamp;
    const apiLatency = Math.round(this.bot.client.ping);
    await botMessage.edit("Ping ? Pong ! Feature useless ? Non ! Puisqu'il y a une latence de " +
        `\`${serverLatency}ms\` sur le serveur.\n` +
        `Pour info, j'ai une latence de \`${apiLatency}ms\` avec vous. :heart:`);
    return true;
  }
}
