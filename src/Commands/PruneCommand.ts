import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Command, ICommandOption } from "./Command";

export class PruneCommand extends Command {
  public commands: Map<string, ICommandOption> = new Map([
    ["prune", {
      channelTypes: [ChannelType.TEXT],
      help: "Supprime un nombre donné de messages d'un membre donné, dans le channel courant (`/prune <count> @user`).",
      roles: ["Tout puissant"],
    }],
  ]);
  public deleteOriginalMessage: boolean = true;

  protected async process(command: string, args: string[], message: Discord.Message): Promise<boolean> {
    if (args.length !== 2 || message.mentions.members.size !== 1) {
      await this.temporaryReplyToMessage(message, "Essaye plutôt sous la forme /prune <count> @user.");
      return true;
    }
    const count = parseInt(args[0], 10);
    if (isNaN(count)) {
      await this.temporaryReplyToMessage(message, "Essaye pas de me la faire à l'envers, je reconnais les nombres.");
      return true;
    }
    if (count < 1 || count > 99) {
      await this.temporaryReplyToMessage(message,
        "Je ne peux supprimer que 1 message au minimum et 99 messages au maximum.");
      return true;
    }
    const member = message.mentions.members.first();
    const messages = (await message.channel.fetchMessages({ limit: count })).filter((m) => m.author.id === member.id);
    if (messages.size === 0) {
      await this.temporaryReplyToMessage(message, "Après vérification, je n'ai trouvé aucun message à supprimer.");
      return true;
    }
    const deletedMessages = await message.channel.bulkDelete(messages);
    await this.temporaryReplyToMessage(message, `J'ai supprimé ${deletedMessages.size} message(s) !`);
    return true;
  }
}
