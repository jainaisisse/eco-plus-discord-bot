import axios from "axios";
import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Constants } from "../Utils/Constants";
import { Command, ICommandOption } from "./Command";

export class RisibankCommand extends Command {
  public commands: Map<string, ICommandOption> = new Map([
    ["risibank", {
      channelTypes: [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM],
      help: "Recherche et envoie un sticker Risibank aléatoire avec les mots-clés passés en paramètre.",
    }],
  ]);
  public deleteOriginalMessage: boolean = true;

  protected async process(command: string, args: string[], message: Discord.Message): Promise<boolean> {
    if (args.length < 1) {
        await this.temporaryReplyToMessage(message, "Essaye plutôt sous la forme `/risibank <mots-clés>`.");
        return true;
    }
    let botMessage = await this.replyToMessage(message,
      "Recherche d'un sticker approprié en cours... :arrows_counterclockwise:");
    if (botMessage instanceof Array) {
      botMessage = botMessage[0];
    }
    // Get the keywords and replace commas with spaces.
    const keywords = args.join(" ").replace(/,/ig, " ");
    // Search for stickers with the given keywords.
    const response = await axios.post(Constants.RISIBANK_API_SEARCH_URL, { search: keywords }, { timeout: 5000 });
    if (response.status !== 200) {
        await botMessage.edit(`${message.author} Il semblerait que le serveur Risibank ` +
            "ne soit pas en forme en ce moment, tu devrais réessayer plus tard.");
        this.registerTemporaryMessages(botMessage);
        return true;
    }
    const results = response.data.stickers;
    if (!results || results.length === 0) {
        await botMessage.edit(`${message.author} On dirait qu'il n'y a aucun sticker ` +
            "qui puisse satisfaire tes envies. :confused:");
        this.registerTemporaryMessages(botMessage);
        return true;
    }
    // Take the first sticker in the results, that's the one with the highest number of views.
    await botMessage.edit(`${message.author} ${results[0].risibank_link} (_${keywords}_)`);
    return true;
  }
}
