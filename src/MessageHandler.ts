import Discord from "discord.js";
import { Bot } from "./Bot";
import { Command } from "./Commands/Command";
import { HelpCommand } from "./Commands/HelpCommand";
import { OmegalulCommand } from "./Commands/OmegalulCommand";
import { PingCommand } from "./Commands/PingCommand";
import { PruneCommand } from "./Commands/PruneCommand";
import { RisibankCommand } from "./Commands/RisibankCommand";
import { CoinFlipTrigger } from "./Triggers/CoinFlipTrigger";
import { DMTrigger } from "./Triggers/DMTrigger";
import { EditTrigger } from "./Triggers/EditTrigger";
import { GoodBotTrigger } from "./Triggers/GoodBotTrigger";
import { MentionTrigger } from "./Triggers/MentionTrigger";
import { RipTrigger } from "./Triggers/RipTrigger";
import { RisitasTrigger } from "./Triggers/RisitasTrigger";
import { Trigger } from "./Triggers/Trigger";
import { YourMomTrigger } from "./Triggers/YourMomTrigger";
import { Logger } from "./Utils/Logger";

const logger = Logger.getLogger("MessageHandler");

export class MessageHandler {
  public commands: Command[];
  public triggers: Trigger[];
  private bot: Bot;

  constructor(bot: Bot) {
    this.bot = bot;
    this.commands = [];
    this.triggers = [];

    this.registerCommands();
    this.registerTriggers();
  }

  public async handleMessage(message: Discord.Message, oldMessage?: Discord.Message) {
    const content = message.content;
    // Check message type.
    if (content.startsWith("/")) {
      // It's a command.
      const args = content.slice(1).trim().split(/ +/g);
      const command = args.shift();
      if (!command) {
        return;
      }

      await this.handleCommand(command.toLowerCase(), args, message, oldMessage);
    } else {
      // It's not a command, search for triggers.
      await this.handleTriggers(message, oldMessage);
    }
  }

  private registerCommands() {
    this.commands.push(new PingCommand(this.bot));
    this.commands.push(new HelpCommand(this.bot));
    this.commands.push(new RisibankCommand(this.bot));
    this.commands.push(new OmegalulCommand(this.bot));
    this.commands.push(new PruneCommand(this.bot));
  }

  private registerTriggers() {
    this.triggers.push(new RisitasTrigger(this.bot));
    this.triggers.push(new YourMomTrigger(this.bot));
    this.triggers.push(new CoinFlipTrigger(this.bot));
    this.triggers.push(new GoodBotTrigger(this.bot));
    this.triggers.push(new RipTrigger(this.bot));

    // These should be the last triggers registered, and their order should not be changed.
    this.triggers.push(new EditTrigger(this.bot));
    this.triggers.push(new MentionTrigger(this.bot));
    this.triggers.push(new DMTrigger(this.bot));

    this.triggers.forEach((trigger) => trigger.compile());
  }

  private async handleCommand(command: string, args: string[], message: Discord.Message, oldMessage?: Discord.Message) {
    for (const commandClass of this.commands) {
      const commandClassName = commandClass.constructor.name;
      if (!commandClass.isMatched(command)) {
        logger.debug(`Command "${command}" NOT handled by ${commandClassName}.`);
        continue;
      }
      const processed = await commandClass.tryProcess(command, args, message, oldMessage);
      if (!processed) {
        logger.debug(`Command "${command}" handled but NOT processed by ${commandClassName}.`);
        break;
      }
      logger.debug(`Command "${command}" handled by ${commandClassName}.`);
      break;
    }
  }

  private async handleTriggers(message: Discord.Message, oldMessage?: Discord.Message) {
    for (const triggerClass of this.triggers) {
      const triggerClassName = triggerClass.constructor.name;
      if (!triggerClass.isMatched(message.content)) {
        logger.debug(`Message NOT handled by ${triggerClassName}.`);
        continue;
      }
      const processed = await triggerClass.tryProcess(message, oldMessage);
      if (!processed) {
        logger.debug(`Message handled but NOT processed by ${triggerClassName}.`);
        continue;
      }
      logger.debug(`Message handled by ${triggerClassName}.`);
      break;
    }
  }
}
