import { readFile } from "fs";
import { Constants } from "../Utils/Constants";
import { IConfiguration } from "./IConfiguration";

export class Configuration {
  public static get configuration(): IConfiguration {
    if (!this.configurationObject) {
      throw new Error("Configuration not loaded.");
    }
    return this.configurationObject;
  }

  public static async loadConfiguration(path: string = Constants.DEFAULT_CONFIG_FILE): Promise<IConfiguration> {
    return new Promise<IConfiguration>((resolve, reject) => {
      readFile(path, "utf-8", (error, data) => {
        if (error) {
          return reject(error);
        }
        this.configurationObject = JSON.parse(data);

        return resolve(this.configurationObject);
      });
    });
  }

  private static configurationObject?: IConfiguration;
}
