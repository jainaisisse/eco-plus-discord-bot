export interface IConfiguration {
  token: string;

  logsDirectory?: string;
}
