import Discord from "discord.js";
import { Configuration } from "./Configuration/Configuration";
import { ChannelType } from "./Discord/ChannelType";
import { MessageHandler } from "./MessageHandler";
import { Logger } from "./Utils/Logger";

const logger = Logger.getLogger("Bot");

export class Bot {
  public client: Discord.Client;
  public messageHandler: MessageHandler;

  constructor() {
    this.client = new Discord.Client();
    this.client.on("ready", this.onReady.bind(this));
    this.client.on("message", this.onMessage.bind(this));
    this.client.on("messageUpdate", this.onMessageUpdate.bind(this));
    this.client.on("messageDelete", this.onMessageDelete.bind(this));
    this.client.on("debug", this.onDebug.bind(this));
    this.client.on("warn", this.onWarn.bind(this));
    this.client.on("disconnect", this.onClientDisconnect.bind(this));
    this.client.on("reconnecting", this.onClientReconnecting.bind(this));
    this.client.on("resume", this.onClientResume.bind(this));
    this.client.on("error", this.onClientError.bind(this));

    this.messageHandler = new MessageHandler(this);
  }

  public async run() {
    logger.info("Starting...");
    await this.client.login(Configuration.configuration.token);
  }

  public async stop() {
    logger.info("Stopping...");
    await this.client.destroy();
  }

  private onReady() {
    logger.info("Connected !");

    if (process.env.NODE_ENV === "development") {
      this.client.guilds.forEach((guild) => {
        logger.debug(`${guild.name} : ${guild.id}`);
        logger.debug("  Channels :");
        guild.channels.forEach((channel) => {
          if (channel.type !== ChannelType.TEXT) {
            return;
          }
          logger.debug(`    ${channel.name} : ${channel.id}`);
        });
        logger.debug("  Members :");
        guild.members.forEach((member) => {
          if (member.user.bot) {
            return;
          }
          logger.debug(`    ${member.displayName} : ${member.id}`);
        });
        logger.debug("  Roles :");
        guild.roles.forEach((role) => {
          logger.debug(`    ${role.name} : ${role.id}`);
        });
        logger.debug("  Emojis :");
        guild.emojis.forEach((emoji) => {
          logger.debug(`    ${emoji.name} : ${emoji.identifier}`);
        });
      });
    }
  }

  private onMessage(message: Discord.Message) {
    // Always skip messages from bots and system.
    if (message.author.bot || message.system) {
      return;
    }
    // Only let messages from text, DM and group DM channels go through.
    if (message.channel.type !== ChannelType.TEXT && message.channel.type !== ChannelType.DM
        && message.channel.type !== ChannelType.GROUP_DM) {
      return;
    }

    logger.trace(`New message. ${message.author.username} : ${message.content}`);

    this.messageHandler.handleMessage(message).catch((error) => {
        logger.error("Error while handling message.", error);
    });
  }

  private onMessageUpdate(oldMessage: Discord.Message, newMessage: Discord.Message) {
    // Always skip messages from bots and system.
    if (newMessage.author.bot || newMessage.system) {
      return;
    }
    // Only let messages from text, DM and group DM channels go through.
    if (newMessage.channel.type !== ChannelType.TEXT && newMessage.channel.type !== ChannelType.DM
        && newMessage.channel.type !== ChannelType.GROUP_DM) {
      return;
    }

    logger.trace(`Message edited. ${newMessage.author.username} : ${oldMessage.content} -> ${newMessage.content}`);

    this.messageHandler.handleMessage(newMessage, oldMessage).catch((error) => {
      logger.error("Error while handling edited message.", error);
    });
  }

  private onMessageDelete(message: Discord.Message) {
    // Always skip messages from bots and system.
    if (message.author.bot || message.system) {
      return;
    }
    // Only let messages from text, DM and group DM channels go through.
    if (message.channel.type !== ChannelType.TEXT && message.channel.type !== ChannelType.DM
        && message.channel.type !== ChannelType.GROUP_DM) {
      return;
    }
    logger.trace(`Message deleted. ${message.author.username} : ${message.content}`);
  }

  private onDebug(info: string) {
    logger.trace("Discord debug.", info);
  }

  private onWarn(info: string) {
    logger.warn("Discord warning.", info);
  }

  private onClientDisconnect() {
    logger.info("Client disconnected.");
  }

  private onClientReconnecting() {
    logger.info("Client reconnecting...");
  }

  private onClientResume(replayed: number) {
    logger.info(`Client resumed (${replayed} events replayed).`);
  }

  private onClientError(error: Error) {
    logger.error("Client error.", error);
  }
}
