export enum ChannelType {
  DM = "dm",
  GROUP_DM = "group",
  TEXT = "text",
  VOICE = "voice",
  CATEGORY = "category",
}
