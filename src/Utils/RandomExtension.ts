export class RandomExtension {
  /**
   * Returns a random number between the first and second given number (both included).
   */
  public static between(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  public static fromArray<T>(array: T[]): T {
    return array[Math.floor(Math.random() * array.length)];
  }
}
