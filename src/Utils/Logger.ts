import log4js from "log4js";
import path from "path";

const DEBUG_LOG_LEVEL = "TRACE";
const PRODUCTION_LOG_LEVEL = "INFO";
const DEFAULT_LOG_LEVEL = process.env.NODE_ENV === "development" ? DEBUG_LOG_LEVEL : PRODUCTION_LOG_LEVEL;
const DEBUG_PATTERN = "%[[%d{ABSOLUTE}][%-5p][%c]%] %m";
const PRODUCTION_PATTERN = "%[[%d{ABSOLUTE}][%-5p]%] %m";
const DEFAULT_PATTERN = process.env.NODE_ENV === "development" ? DEBUG_PATTERN : PRODUCTION_PATTERN;
const DEFAULT_LOGS_DIRECTORY = "./logs";
const DEFAULT_LOGS_FILE_NAME = "default.txt";

export class Logger {
  public static async setupLogger(logsDirectory: string = DEFAULT_LOGS_DIRECTORY,
                                  logsFileName: string = DEFAULT_LOGS_FILE_NAME) {
    const logsFilePath = path.resolve(logsDirectory, logsFileName);
    return new Promise((resolve, reject) => {
      log4js.shutdown((error) => {
        if (error) {
          return reject(error);
        }
        log4js.configure({
          appenders: {
            file: {
              backups: 1,
              filename: logsFilePath,
              layout: {
                pattern: DEBUG_PATTERN,
                type: "pattern",
              },
              maxLogSize: 10485760,
              type: "file",
            },
            out: {
              layout: {
                pattern: DEFAULT_PATTERN,
                type: "pattern",
              },
              type: "stdout",
            },
            /* tslint:disable object-literal-sort-keys */
            // This has to be the last entry, because log4js is shit, but it's still awesome.
            console: { type: "logLevelFilter", level: DEFAULT_LOG_LEVEL, appender: "out" },
            /* tslint:enable object-literal-sort-keys */
          },
          categories: {
            default: { appenders: ["console", "file"], level: DEBUG_LOG_LEVEL },
          },
        });
        return resolve();
      });
    });
  }

  public static getLogger(category?: string): log4js.Logger {
    return log4js.getLogger(category);
  }

  public static async shutdown() {
    return new Promise((resolve, reject) => {
      log4js.shutdown((error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }
}
