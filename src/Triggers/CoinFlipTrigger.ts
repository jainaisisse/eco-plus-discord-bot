import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { RandomExtension } from "../Utils/RandomExtension";
import { Trigger } from "./Trigger";

export class CoinFlipTrigger extends Trigger {
  protected trigger: string | RegExp = "pile ou face|face ou pile";
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message): Promise<boolean> {
    if (RandomExtension.between(0, 100) === 100) {
      await message.reply("Tranche ! La tuile...");
      return true;
    }
    if (RandomExtension.between(1, 100) < 50) {
      await message.reply("Pile !");
    } else {
      await message.reply("Face !");
    }
    return true;
  }
}
