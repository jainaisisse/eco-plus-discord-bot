import axios from "axios";
import cheerio from "cheerio";
import Discord from "discord.js";
import he from "he";
import { ChannelType } from "../Discord/ChannelType";
import { Constants } from "../Utils/Constants";
import { Logger } from "../Utils/Logger";
import { RandomExtension } from "../Utils/RandomExtension";
import { Trigger } from "./Trigger";

const logger = Logger.getLogger("YourMomTrigger");

export class YourMomTrigger extends Trigger {
  protected trigger: string | RegExp = "ta mère|ta mere|vos mères|vos mères";
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message): Promise<boolean> {
    const firstPage = await axios.get(Constants.YOUR_MOM_JOKES_URL, {
      responseType: "text",
      timeout: 5000,
    });
    if (firstPage.status !== 200) {
      return false;
    }
    logger.trace("First page data.", firstPage.data);
    let $ = cheerio.load(firstPage.data);
    const pageCountString = $("div#pagesN strong").last().html();
    if (!pageCountString) {
      return false;
    }
    const pageCount = parseInt(pageCountString, 10);
    logger.debug("Page count.", pageCount);
    const randomPage = RandomExtension.between(1, pageCount);
    const jokesPage = await axios.get(Constants.YOUR_MOM_JOKES_URL, {
      params: { p: randomPage },
      responseType: "text",
      timeout: 5000,
    });
    if (jokesPage.status !== 200) {
      return false;
    }
    logger.trace("Jokes page data.", jokesPage.data);
    $ = cheerio.load(jokesPage.data);
    const jokes: string[] = $("div#content div#cont h4.texte").map((i, el) => $(el).html()).get();
    logger.trace("Jokes.", jokes);
    if (!jokes || jokes.length === 0) {
      return false;
    }
    const joke = he.decode(RandomExtension.fromArray(jokes).trim());
    await message.reply(joke);
    return false;
  }
}
