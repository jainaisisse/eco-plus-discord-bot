import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { RandomExtension } from "../Utils/RandomExtension";
import { Trigger } from "./Trigger";

export class DMTrigger extends Trigger {
  protected trigger: string | RegExp = /.*/;
  protected channelTypes: ChannelType[] = [ChannelType.DM];

  public async process(message: Discord.Message): Promise<boolean> {
    const responses = [
      "Désolé de ne pas te répondre très souvent, mais je t'écoute !",
      "Mais encore ?",
      "Très intéressant dis-moi !",
      "J'ai rien compris... Peut-être parce que j'suis un bot ? :thinking:",
    ];
    if (RandomExtension.between(1, 100) > 90) {
      await message.reply(RandomExtension.fromArray(responses));
    }
    return true;
  }
}
