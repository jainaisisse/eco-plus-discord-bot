import Discord from "discord.js";
import levenshtein from "js-levenshtein";
import { ChannelType } from "../Discord/ChannelType";
import { Logger } from "../Utils/Logger";
import { Trigger } from "./Trigger";

const logger = Logger.getLogger("EditTrigger");

export class EditTrigger extends Trigger {
  protected trigger: string | RegExp = /.*/;
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message, oldMessage?: Discord.Message): Promise<boolean> {
    if (!oldMessage) {
      return false;
    }
    // If the previous message was empty (eg. only a photo uploaded), skip the trigger.
    if (!oldMessage.content.trim()) {
      return false;
    }
    const distance = levenshtein(oldMessage.content, message.content);
    const longestMessageLength = Math.max(oldMessage.content.length, message.content.length);
    const modifyPercentage = 100 - Math.round(((longestMessageLength - distance) / longestMessageLength) * 100);
    if (modifyPercentage < 90) {
      logger.debug(`Modification percentage : ${modifyPercentage}%`);
      return false;
    }
    const authorName = message.member ? message.member.displayName : message.author.username;
    const embed = new Discord.RichEmbed()
      .setTitle(`Ancien message de ${authorName}`)
      .setDescription(oldMessage);
    await message.reply(
      `Tu as modifié ton message à ${modifyPercentage}%, c'est plutôt beaucoup je trouve. ` +
      "Je remets ton ancien message, dans le doute. Cordialement, Poucave éco+. :kissing_smiling_eyes:",
      { embed },
    );
    return true;
  }
}
