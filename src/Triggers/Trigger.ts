import Discord from "discord.js";
import { Bot } from "../Bot";
import { ChannelType } from "../Discord/ChannelType";

export abstract class Trigger {
  protected bot: Bot;
  protected abstract trigger: RegExp | string;
  protected roles: string[] | undefined;
  protected channels: string[] | undefined;
  protected abstract channelTypes: ChannelType[];

  constructor(bot: Bot) {
    this.bot = bot;
  }

  public compile() {
    if (typeof this.trigger !== "string" && !(this.trigger instanceof RegExp)) {
      throw new Error("The trigger must be a string or a RegExp instance.");
    }
    if (typeof this.trigger === "string") {
      this.trigger = new RegExp(`(?:^|\\W)(?:${this.trigger})(?:$|\\W)`, "i");
    }
  }

  public isMatched(message: string): boolean {
    if (!(this.trigger instanceof RegExp)) {
      throw new Error("No trigger set.");
    }
    return this.trigger.test(message);
  }

  public async tryProcess(message: Discord.Message, oldMessage?: Discord.Message): Promise<boolean> {
    // Check for channel type condition.
    const channelTypes = this.channelTypes;
    if (channelTypes && channelTypes.length > 0 && !channelTypes.includes(message.channel.type as ChannelType)) {
      return false;
    }
    // Check for roles condition.
    const roles = this.roles;
    if (roles && roles.length > 0 && !message.member.roles.some((r) => roles.includes(r.name))) {
      return false;
    }
    // Check for channel condition.
    const channels = this.channels;
    const messageChannelName = (message.channel as Discord.TextChannel).name;
    if (channels && channels.length > 0 && !channels.includes(messageChannelName)) {
      return false;
    }

    return this.process(message, oldMessage);
  }

  public abstract async process(message: Discord.Message, oldMessage?: Discord.Message): Promise<boolean>;
}
