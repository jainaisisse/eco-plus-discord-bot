import Discord from "discord.js";
import { ChannelType } from "../Discord/ChannelType";
import { Trigger } from "./Trigger";

export class GoodBotTrigger extends Trigger {
  protected trigger: string | RegExp = "good bot|bon bot";
  protected channelTypes: ChannelType[] = [ChannelType.TEXT, ChannelType.DM, ChannelType.GROUP_DM];

  public async process(message: Discord.Message): Promise<boolean> {
    await message.react("😊");
    return true;
  }
}
